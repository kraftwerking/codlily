package com.kraftwerking.codlily.solution;

import java.util.ArrayList;

//you can also use imports, for example:
//import java.util.*;

//you can write to stdout for debugging purposes, e.g.
//System.out.println("this is a debug message");

public class Solution2 {	
	
	public boolean solution(String s, String t) {
		// write your code in Java SE 8
		System.out.println("s " + s);
		System.out.println("t " + t);
		
		String sMod = getModString(s);
		String tMod = getModString(t);
		boolean match = false;
		
		if(sMod.length()==tMod.length()){
			for(int i=0;i<sMod.length();i++){
				if(sMod.charAt(i) == tMod.charAt(i)){
					match=true;
					continue;
				} else if (sMod.charAt(i) == '?' || tMod.charAt(i) == '?'){
					match = true;
					continue;
				} else {
					match=false;
					break;
				}
			}			
		}
		
		return match;
	}


	private String getModString(String s) {
		// TODO Auto-generated method stub
		int len = s.length();
		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();

		for(int i=0;i<len;i++){
			
			if(Character.isDigit(s.charAt(i))){
				sb.append(s.charAt(i));
			} else {
				if(sb.length() >0){
					//we have a number of ?
					Integer numQs = Integer.parseInt(sb.toString());
					System.out.println("numQs " + numQs);
					for(int x=0;x<numQs;x++){
						sb2.append("?");
					}
				} 
				
				sb = new StringBuilder();
				sb2.append(s.charAt(i));

			}

		}
		
		if(sb.length() >0){
			//we have a number of ?
			Integer numQs = Integer.parseInt(sb.toString());
			System.out.println("numQs " + numQs);
			for(int x=0;x<numQs;x++){
				sb2.append("?");
			}
		} 
		
		sb = new StringBuilder();
		
		return sb2.toString();
	}


}