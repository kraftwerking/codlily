package com.kraftwerking.codlily.solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

//you can also use imports, for example:
//import java.util.*;

//you can write to stdout for debugging purposes, e.g.
//System.out.println("this is a debug message");

public class Solution {	
	
	private static ArrayList<Integer> maxUniqueXs = new ArrayList<Integer>();
	private static int rootX = 0;
	
	public int solution(Tree t) {
		// write your code in Java SE 8
		ArrayList<Integer> path = new ArrayList<Integer>();
		rootX = t.x;
		getAllPathsToLeaf(t,path);
		return Collections.max(maxUniqueXs);

	}

	// Prints all paths to leaf  
	 public static void getAllPathsToLeaf(Tree t, ArrayList<Integer> path) {  
	     if ( t == null )  
	         return;  
	  
	     // storing path x in array  
	     path.add(t.x);  
	  
	     if(t.l == null && t.r == null) {  
	         // leaf node is reached  
	    	 System.out.println("leaf node has been reached");
	    	 Set<Integer> uniqueXs = new HashSet<Integer>(path);
	    	 maxUniqueXs.add(uniqueXs.size());
	    	 path.clear();
	    	 path.add(rootX);
	         return;  
	     }  
	  
	     getAllPathsToLeaf(t.l, path);  
	     getAllPathsToLeaf(t.r, path);  
	 }  

}