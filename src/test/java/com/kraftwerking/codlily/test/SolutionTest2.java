package com.kraftwerking.codlily.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.kraftwerking.codlily.solution.Solution2;

public class SolutionTest2 {


	//@Before
    public void runBeforeTestMethod() {
        System.out.println("@Before - runBeforeTestMethod");
    }
	
	//@Test
    public void testSolution() {
        System.out.println("@Test - testSolution");
        Solution2 solution2 = new Solution2();
        String s = "A2Le";
        String t = "2pL1";
        boolean result = solution2.solution(s, t);
        assertTrue("result is true",result);
    }
	
	//@Test
    public void testSolution2() {
        System.out.println("@Test - testSolution");
        Solution2 solution2 = new Solution2();
        String s = "a10";
        String t = "10a";
        boolean result = solution2.solution(s, t);
        assertTrue("result is true",result);
    }
	
	@Test
    public void testSolution3() {
        System.out.println("@Test - testSolution");
        Solution2 solution2 = new Solution2();
        String s = "ba1";
        String t = "1Ad";
        boolean result = solution2.solution(s, t);
        assertFalse("result is true",result);
    }

	
}
