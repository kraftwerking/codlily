package com.kraftwerking.codlily.test;

import org.junit.Test;
import com.kraftwerking.codlily.solution.Tree;
import com.kraftwerking.codlily.solution.Solution;
import static org.junit.Assert.*;
import org.junit.Before;

public class SolutionTest {
	
    private Tree g = new Tree();
    private Tree d = new Tree();
    private Tree b = new Tree();
    private Tree e = new Tree(); 
    private Tree f = new Tree(); 
    private Tree c = new Tree(); 
    private Tree a = new Tree(); 

	@Before
    public void runBeforeTestMethod() {
        System.out.println("@Before - runBeforeTestMethod");
        g.x=5;
        g.l=null;
        g.r=null;
        
        d.x=4;
        d.l=g;
        d.r=null;
        
        b.x=5;
        b.l=d;
        b.r=null;
        
        e.x=1;
        e.l=null;
        e.r=null;
        
        f.x=6;
        f.l=null;
        f.r=null;
        
        c.x=6;
        c.l=e;
        c.r=f;
        
        a.x=4;
        a.l=b;
        a.r=c;
        
    }
	
	@Test
    public void testSolution() {
        System.out.println("@Test - testSolution");
        Solution s = new Solution();
        int res = s.solution(a);
        assertTrue(res==3);
    }

	
}
